package model;

public class Vehicle implements Comparable<Vehicle> {
    private final String brand;
    private final String model;
    private final int price;

    public Vehicle(String brand, String model, int price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", ";
    }

    @Override
    public int compareTo(Vehicle vehicle) {
        return this.price - vehicle.price;
    }
}