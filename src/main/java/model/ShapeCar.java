package model;

public enum ShapeCar {
    COUPE,
    SEDAN,
    WAGON
}