package model;

public class Tractor extends Vehicle {
    private int maxPulledWeight;

    public Tractor(String brand, String model, int price, int maxPulledWeight) {
        super(brand, model, price);
        this.maxPulledWeight = maxPulledWeight;
    }

    public int getMaxPulledWeight() {
        return maxPulledWeight;
    }

    @Override
    public String toString() {
        return "Tractor{" + super.toString() +
                "maxPulledWeight=" + maxPulledWeight +
                "} ";
    }
}