package model;

public enum ShapeMotorcycle {
    CHOPPER,
    CRUISER,
    ENDURO
}