package model;

public class Motorcycle extends Vehicle {
    private final int topSpeed;
    private final ShapeMotorcycle shapeMotorcycle;

    public Motorcycle(String brand, String model, int price, int topSpeed, ShapeMotorcycle shapeMotorcycle) {
        super(brand, model, price);
        this.topSpeed = topSpeed;
        this.shapeMotorcycle = shapeMotorcycle;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public ShapeMotorcycle getShapeMotorcycle() {
        return shapeMotorcycle;
    }

    @Override
    public String toString() {
        return "Motorcycle{" + super.toString() +
                "topSpeed=" + topSpeed +
                ", shapeMotorcycle=" + shapeMotorcycle +
                "} ";
    }
}