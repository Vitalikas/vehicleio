package model;

public enum VehicleType {
    Car,
    Motorcycle,
    Tractor
}