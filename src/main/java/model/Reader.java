package model;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Reader {
    public List<CSVRecord> readFile(InputStream file) {
        List<CSVRecord> records = new ArrayList<>();
        try(CSVParser csvParser = new CSVParser(new BufferedReader(new InputStreamReader(file)),
                CSVFormat.DEFAULT.withIgnoreSurroundingSpaces())) {
            records = csvParser.getRecords();
        } catch (IOException e) {
            e.printStackTrace();
        } return records;
    }
}