package model;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Writer {
    public void writeCarsToFile(List<Vehicle> vehicles) {
        try (
                CSVPrinter csvPrinter = new CSVPrinter(new BufferedWriter(
                        new FileWriter(System.getProperty("user.home") + "/Desktop/cars.txt")),
                        CSVFormat.DEFAULT.withHeader(
                                Field.BRAND.toString(),
                                Field.MODEL.toString(),
                                Field.PRICE.toString(),
                                Field.TOPSPEED.toString(),
                                Field.TRANSMISSION.toString(),
                                Field.SHAPE.toString()))
        ) {
            vehicles.stream()
                    .filter(vehicle -> vehicle instanceof Car)
                    .forEach(vehicle -> {
                        try {
                            csvPrinter.printRecord(vehicle.getBrand(),
                                    vehicle.getModel(),
                                    String.valueOf(vehicle.getPrice()),
                                    String.valueOf(((Car) vehicle).getTopSpeed()),
                                    String.valueOf(((Car) vehicle).getTransmissionCar()),
                                    String.valueOf(((Car) vehicle).getShapeCar()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void writeMotorcyclesToFile(List<Vehicle> vehicles) {
        try (
                CSVPrinter csvPrinter = new CSVPrinter(new BufferedWriter(
                        new FileWriter(System.getProperty("user.home") + "/Desktop/motorcycles.txt")),
                        CSVFormat.DEFAULT.withHeader(
                                Field.BRAND.toString(),
                                Field.MODEL.toString(),
                                Field.PRICE.toString(),
                                Field.TOPSPEED.toString(),
                                Field.SHAPE.toString()))
        ) {
            vehicles.stream()
                    .filter(vehicle -> vehicle instanceof Motorcycle)
                    .forEach(vehicle -> {
                        try {
                            csvPrinter.printRecord(vehicle.getBrand(),
                                    vehicle.getModel(),
                                    String.valueOf(vehicle.getPrice()),
                                    String.valueOf(((Motorcycle) vehicle).getTopSpeed()),
                                    String.valueOf(((Motorcycle) vehicle).getShapeMotorcycle()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void writeTractorsToFile(List<Vehicle> vehicles) {
        try (
                CSVPrinter csvPrinter = new CSVPrinter(new BufferedWriter(
                        new FileWriter(System.getProperty("user.home") + "/Desktop/tractors.txt")),
                        CSVFormat.DEFAULT.withHeader(
                                Field.BRAND.toString(),
                                Field.MODEL.toString(),
                                Field.PRICE.toString(),
                                Field.MAXPULLEDWEIGHT.toString()))
        ) {
            vehicles.stream()
                    .filter(vehicle -> vehicle instanceof Tractor)
                    .forEach(vehicle -> {
                        try {
                            csvPrinter.printRecord(vehicle.getBrand(),
                                    vehicle.getModel(),
                                    String.valueOf(vehicle.getPrice()),

                                    String.valueOf(((Tractor) vehicle).getMaxPulledWeight()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}