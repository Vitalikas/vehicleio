package model;

public enum TransmissionCar {
    MANUAL,
    AUTOMATIC
}