package model;

public class Car extends Vehicle {
    private final int topSpeed;
    private final TransmissionCar transmissionCar;
    private final ShapeCar shapeCar;

    public Car(String brand, String model, int price,
               int topSpeed, TransmissionCar transmissionCar, ShapeCar shapeCar) {
        super(brand, model, price);
        this.topSpeed = topSpeed;
        this.transmissionCar = transmissionCar;
        this.shapeCar = shapeCar;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public TransmissionCar getTransmissionCar() {
        return transmissionCar;
    }

    public ShapeCar getShapeCar() {
        return shapeCar;
    }

    @Override
    public String toString() {
        return "Car{" + super.toString() +
                "topSpeed=" + topSpeed +
                ", transmissionCar=" + transmissionCar +
                ", shapeCar=" + shapeCar +
                "} ";
    }
}