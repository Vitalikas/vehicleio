import model.*;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {
    public static void main(String[] args) {
        // 1. Read vehicles.txt
        System.out.println("Reading file vehicles.txt ...");
        // Getting file path from folder "resources"
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream is = classLoader.getResourceAsStream("vehicles.txt");
        // Reading file and getting all records
        List<CSVRecord> records = getRecords(is);

        // 2. Create objects of the proper type
        System.out.println("Creating objects of the proper type ...");
        List<Vehicle> vehicles = createVehicleObj(records);

        System.out.println("Objects created from text file:");
        vehicles.forEach(System.out::println);

        // 3. Count the number of cars, motorcycles, tractors
        long carCount = vehicles.stream()
                .filter(vehicle -> vehicle instanceof Car)
                .count();
        System.out.println("\nNumber of cars: " + carCount);
        System.out.println("Cars: ");
        vehicles.stream()
                .filter(vehicle -> vehicle instanceof Car)
                .forEach(System.out::println);

        long motorcycleCount = vehicles.stream()
                .filter(vehicle -> vehicle instanceof Motorcycle)
                .count();
        System.out.println("\nNumber of motorcycles: " + motorcycleCount);
        System.out.println("Motorcycles: ");
        vehicles.stream()
                .filter(vehicle -> vehicle instanceof Motorcycle)
                .forEach(System.out::println);

        long tractorCount = vehicles.stream()
                .filter(vehicle -> vehicle instanceof Tractor)
                .count();
        System.out.println("\nNumber of tractors: " + tractorCount);
        System.out.println("Tractors: ");
        vehicles.stream()
                .filter(vehicle -> vehicle instanceof Tractor)
                .forEach(System.out::println);

        // 4. Count how many vehicles of each brand are there
        System.out.println("\nCount how many vehicles of each brand are there:");
        Map<String, Integer> map = new HashMap<>();
        vehicles.forEach(vehicle -> {
            if (map.containsKey(vehicle.getBrand())) {
                Integer brandCount = map.get(vehicle.getBrand());
                brandCount++;
                map.put(vehicle.getBrand(), brandCount);
            } else {
                map.put(vehicle.getBrand(), 1);
            }
        });
        map.forEach((brand, count) -> System.out.println("Brand: " + brand + ", count: " + count));

        // 5. Sort the cars by price
        System.out.println("\nSorting cars by price ...");
        vehicles.stream()
                .filter(vehicle -> vehicle instanceof Car)
                .sorted()
                .forEach(System.out::println);

        // 6. Sort the choppers by top speed
        System.out.println("\nSorting choppers by top speed ...");
        vehicles.stream()
                .filter(vehicle -> vehicle instanceof Motorcycle)
                .sorted(Comparator.comparing(vehicle -> ((Motorcycle) vehicle).getTopSpeed()))
                .filter(vehicle -> ((Motorcycle) vehicle).getShapeMotorcycle().equals(ShapeMotorcycle.CHOPPER))
                .forEach(System.out::println);

        // 7. Display each category of vehicles in separate files
        System.out.println("\nPrinting to files...");
        Writer writer = new Writer();

        // To write files simultaneously use ExecutorService
        ExecutorService executor = Executors.newFixedThreadPool(3);
        // create task list
        List tasks = new ArrayList<>();
        // add tasks to list
        tasks.add((Callable<Void>) () -> {
            writer.writeCarsToFile(vehicles);
            return null;
        });
        tasks.add((Callable<Void>) () -> {
            writer.writeMotorcyclesToFile(vehicles);
            return null;
        });
        tasks.add((Callable<Void>) () -> {
            writer.writeTractorsToFile(vehicles);
            return null;
        });
        try {
            executor.invokeAll(tasks);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executor.shutdown();
    }

    private static int setPrice() {
        System.out.print("Price not found, enter new price: ");
        return new Scanner(System.in).nextInt();
    }

    private static List<CSVRecord> getRecords(InputStream inputStream) {
        return new Reader().readFile(inputStream);
    }

    private static List<Vehicle> createVehicleObj(List<CSVRecord> records) {
        Logger logger = Logger.getLogger("log");
        try {
            FileHandler fh = new FileHandler("errors.log", true);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }

        List<Vehicle> vehicles = new ArrayList<>();
        records.forEach(strings -> {
            try {
                if (strings.get(0).equals(VehicleType.Car.toString())) {
                    vehicles.add(new Car(strings.get(1), strings.get(2), Integer.parseInt(strings.get(3)),
                            Integer.parseInt(strings.get(4)), TransmissionCar.valueOf(strings.get(5)),
                            ShapeCar.valueOf(strings.get(6))));
                } else if (strings.get(0).equals(VehicleType.Motorcycle.toString())) {
                    vehicles.add(new Motorcycle(strings.get(1), strings.get(2), Integer.parseInt(strings.get(3)),
                            Integer.parseInt(strings.get(4)), ShapeMotorcycle.valueOf(strings.get(5))));
                } else if (strings.get(0).equals(VehicleType.Tractor.toString())) {
                    vehicles.add(new Tractor(strings.get(1), strings.get(2), Integer.parseInt(strings.get(3)),
                            Integer.parseInt(strings.get(4))));
                }
            } catch (Exception e) {
                logger.warning("Error creating list of objects! Error: " + e +
                        "\nCheck file vehicles.txt for field validations!" +
                        "\nYou can handle all exceptions in this code block.");
                vehicles.add(new Tractor(strings.get(1), strings.get(2), setPrice(),
                        Integer.parseInt(strings.get(4))));
            }
        });
        return vehicles;
    }
}